import java.util.ArrayList;

public class Corridor 
{
  public ArrayList<coordinates> tiles = new ArrayList<coordinates>();
  private Room r1;
  private Room r2;

  public Corridor(Room r1, Room r2)
  {
    this.r1 = r1;
    this.r2 = r2;
    this.drawCorridor();
  }

  private void drawCorridor()
  {
    coordinates start = r1.tiles.get(0);
    coordinates finish = r2.tiles.get(0);
    int x = finish.x - start.x;
    int y = finish.y - start.y;
    int ix;
    int iy;
    if (x < 0) ix = -1;
    else ix = 1;
    if (y < 0) iy = -1;
    else iy = 1;
    for (int i = 0; i < Math.abs(x); ++i)
    {
      this.tiles.add(new coordinates(start.x + i*ix, start.y));
    }
    for (int i = 0; i < Math.abs(y); ++i)
    {
      this.tiles.add(new coordinates(start.x + x, start.y + i*iy));
    }
  }
}
