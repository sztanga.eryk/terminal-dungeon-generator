import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;

public class Linker 
{
  private LinkedList<Room> rooms;

  public Linker(LinkedList<Room> rooms)
  {
    this.rooms = rooms;
  }

  public void link(Room r1, Room r2)
  {
    if (this.areLinked(r1, r2)) return;
    if (!r1.links.contains(r2))
    {
      r1.links.add(r2);
      r2.links.add(r1);
    }
  }

  public boolean areLinked(Room r1, Room r2)
  {
    if (r2 == null || r1 == null) return true;
    else return false;
  }

  public boolean hasLinks(Room r)
  {
    if (r.links.size() > 0) return true;
    else return false;
  }

  public void linkAll()
  {
    class DistComparator implements Comparator<Room>
    {
      private Room r;
      public DistComparator(Room r)
      {
        this.r = r;
      }
      @Override
      public int compare(Room r1, Room r2) 
      {
        return Double.compare(Linker.dist(this.r, r1), Linker.dist(this.r, r2));
      }
    }

    Room room = this.rooms.getFirst();
    for (int i = 0; i < this.rooms.size(); ++i)
    {
      LinkedList<Room> otherRooms = (LinkedList<Room>) this.rooms.clone();
      otherRooms.remove(room);
      DistComparator comp = new DistComparator(room);
      Collections.sort(otherRooms, comp);
      Room otherRoom;
      do
      {
        otherRoom = otherRooms.pollLast();
      }while(otherRoom != null && this.hasLinks(otherRoom) && !this.areLinked(room, otherRoom));
      this.link(room, otherRoom);
      room = otherRoom;
    }

    
  }

  public static double dist(Room r1, Room r2)
  {
    double squareDis = Math.pow(r1.tiles.get(0).x - r2.tiles.get(0).x, 2) + Math.pow(r1.tiles.get(0).y - r2.tiles.get(0).y, 2);
    return squareDis;
  }
}
