import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Random;

public class Room
{
  public ArrayList<coordinates> tiles = new ArrayList<coordinates>();
  public LinkedList<Room> links = new LinkedList<Room>();
  public int id;
  private collider xCol;
  private collider yCol;
  public Room(int id, int maxX, int maxY)
  {
    this.id = id;
    Random rand = new Random();
    int len = rand.nextInt(2, maxX/3);
    int hig = rand.nextInt(2, maxY/3);
    int x = rand.nextInt(0, maxX - len + 1);
    int y = rand.nextInt(0, maxY - hig + 1);
    this.xCol = new collider(x -1, x + len);
    this.yCol = new collider(y- 1, y + hig);
    for (int i = x; i < x+len; i++)
    {
      for (int j = y; j < y+hig; j++)
      {
        this.tiles.add(new coordinates(i, j));
      }
    }
  }
  public boolean collide(Room other)
  {
    if (this.xCol.collide(other.xCol) && this.yCol.collide(other.yCol))
    {
      return true;
    }
    else
    {
      return false;
    }
  }
}
