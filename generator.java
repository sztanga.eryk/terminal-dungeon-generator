import java.util.ArrayList;
import java.util.LinkedList;

public class generator {
  private char[][] map;
  private int size;
  private LinkedList<Room> rooms = new LinkedList<Room>();
  private LinkedList<Corridor> corridors = new LinkedList<Corridor>();

  public generator(int size)
  {
    this.size = size;
    this.map = new char[size][size];
    this.init();
  }

  public void print()
  {
    for (int i=0; i<this.size;i++)
    {
      for (int j=0; j<this.size;j++)
      {
        System.out.print(String.format("%1$3c", this.map[i][j]));
      }
      System.out.print("\n");
    }
  }

  private void init()
  {
    for (int i=0; i<this.size;i++)
    {
      for (int j=0; j<this.size;j++)
      {
        this.map[i][j] = '0';
      }
    }
  }

  public void generateRoom(int numberOfRooms)
  {
    Room r;
    for(int j = 0; j < numberOfRooms; j++)
    {
      int i =0;
      do
      {
        r = new Room(j, this.size, this.size);
        i++;
        if (i>100)
        {
          r.tiles.clear();
          break;
        }
      }while (this.collide(r));
      this.addRoom(r);
      this.adjustChars(r.tiles);
    }
    // this.print();
    this.linkRooms();
    this.generateCorridors();
  }

  private void generateCorridors()
  {
    Room room = this.rooms.getFirst();
    for (int i = 0; i < this.rooms.size() - 1; ++i)
    {
      Room otherRoom = room.links.getLast();
      Corridor corr = new Corridor(room, otherRoom);
      this.corridors.add(corr);
      room = otherRoom;
      this.adjustChars(corr.tiles);
    }
  }


  private boolean collide(Room r)
  {
    for (Room room : this.rooms)
    {
      if (room.collide(r)) return true;
    }
    return false;
  }

  private void adjustChars(ArrayList<coordinates> tiles)
  {
    for (coordinates c : tiles)
    {
      this.map[c.x][c.y] = '.';
    }
  }

  private void addRoom(Room r)
  {
    if (r.tiles.size() != 0)
    {
      this.rooms.add(r);
    }
  }

  public void linkRooms()
  {
    Linker linker = new Linker(this.rooms);
    linker.linkAll();
  }

  public void debugLink()
  {
    for (Room room : this.rooms)
    {
      String links = "";
      for (Room link: room.links)
      {
        links = links + link.id + " ";
      }
      System.out.print(room.id + ": " + links + "\n");
    }
  }
}
